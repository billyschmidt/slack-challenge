# Slack-challenge

A simple tool that allows the deployment of a PHP application. This tool uses mainly fabric, docker and docker-compose to setup an environment with PHP7-fpm, MariaDB and Nginx. I chose these tools because it allows for a highly configurable and scalable design pattern.

## Usage
~~~
The only prerequisite is Python. Then just clone down this repo and go to the repository root. Then make a virtual environment (if you choose) and install the requirements. After that you're ready to start using it!
$ pip install -r requirements.txt
$ fab -l
Available commands:

    check_status            Check status of running apps.
    install_docker          Install docker and make sure it's started.
    install_docker_compose  Install docker-compose.
    install_package         Install package using apt-get.
    run_new_deploy          Runs a new deploy to all hosts.
    sendfile                Send file to remote hosts.
    update                  Do an apt-get update and install git.
    update_app              Update code and reload the apps.
    vagrant                 Run the following tasks on a vagrant box.

All that's really needed is to run:
$ fab run_new_deploy
which will deploy our app to the two hosts 54.236.20.81 and 52.73.99.202.

As another example, to restart the apps after pushing a new change:
$ fab update_app

By default tasks will be performed on the hosts specified in fabfile (hosts.env). Otherwise they hosts can be passed as arguments on the command line like so:
$ fab run_new_deploy -H host1, host2, host3

To install packages use the install_packages task:
$ fab install_package:mercurial

To specify files and metadata (permissions) use the send_file task, which can be executed in the form send_file:localpath,remotepath,mod)
$ fab send_file:newfile1,/tmp/newfile1,755

Testing was done with vagrant, which is super easy. Just make sure vagrant and the box you are testing with is installed, then run:
$ vagrant up
$ fab vagrant run_new_deploy

~~~

### Structure

~~~

├── Vagrantfile
├── fabfile.py
├── app
│   └── public
│       └── index.php
├── database
├── docker-compose.yml
├── fpm
│   ├── Dockerfile
│   └── supervisord.conf
├── nginx
│   ├── Dockerfile
│   └── default.conf
~~~

- `app` is the directory for project files. Our Nginx config is pointing to `app/public`, which can be changed in `nginx/default.conf`
- `Vagrantfile` is for testing the fabfile locally.
- `fabfile.py` is our fabfile.
- `database` is where MariDB will store the database files.
