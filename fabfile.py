from fabric.api import (env, settings, run, sudo,
                        cd, parallel, task, put)
from fabric.contrib.files import exists
from fabtools.vagrant import vagrant
from fabtools.service import is_running, start

env.hosts = ['54.236.20.81', '52.73.99.202']
env.user = 'root'
env.password = 'foobarbaz'

repo_url = 'https://gitlab.com/billyschmidt/slack-challenge'
code_path = '$HOME/slack-challenge/'


@task
@parallel
def install_package(package_name):
    """Install package using apt-get."""
    sudo('apt-get -y install {}'.format(package_name))


@task
@parallel
def update():
    """Do an apt-get update and install git."""
    sudo('apt-get -y update')
    sudo('apt-get -y install git')


@task
@parallel
def install_docker():
    """Install docker and make sure it's started."""
    if not exists(code_path):
        run('mkdir -p {}'.format(code_path))
    with settings(warn_only=True):
        with cd(code_path):
            sudo('curl -sSL https://get.docker.com/ | sh')
            start('docker')
            run('sudo gpasswd -a $USER docker')


@task
@parallel
def install_docker_compose():
    """Install docker-compose."""
    sudo('apt-get -y install python-pip')
    sudo('pip install docker-compose')
    if not is_running('docker'):
        start('docker')


@parallel
def clone_repo():
    """Clone down the repository."""
    if not exists(code_path):
        run('mkdir -p {}'.format(code_path))
    with cd(code_path):
        with settings(warn_only=True):
            run('git clone {} .'.format(repo_url))


@task
@parallel
def run_new_deploy():
    """Runs a new deploy to all hosts."""
    update()
    clone_repo()
    install_docker()
    install_docker_compose()
    deploy_app()
    check_status()


@parallel
def deploy_app():
    """Deploy app to all hosts."""
    with cd(code_path):
        with settings(warn_only=True):
            sudo('docker-compose up -d')


@task
@parallel
def update_app():
    """Update code and restart the containers."""
    with cd(code_path):
        with settings(warn_only=True):
            run('git pull')
            sudo('docker-compose restart')


@task
@parallel
def check_status():
    """Check status of running apps."""
    with cd(code_path):
        with settings(warn_only=True):
            sudo('docker ps')


@task
@parallel
def sendfile(localpath, remotepath, mod):
    """Send file to remote hosts."""
    put(localpath, remotepath, mode=int(mod, 8), use_sudo=True)
